package com.redhat.route;

import org.apache.camel.builder.RouteBuilder;
import static com.redhat.constant.HTTPConstant.*;

public class RestKafkaRoute extends RouteBuilder {

    @Override
    public void configure() throws Exception {

        rest("/kafka")
        .consumes(TEXT_PLAIN)
        .produces(TEXT_PLAIN)
        .post()
        .to("direct:remove-header");

        from("direct:remove-header")
            .routeId("rest-route")
            .log("sending message.")
            .removeHeaders("*")
            .to("{{kafka.uri}}")
            .removeHeaders("*")
            .setBody(constant("message sent."))
            .log("${body}");

    }
}